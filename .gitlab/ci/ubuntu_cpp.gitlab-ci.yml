include:
  - local: '/.gitlab/ci/default.gitlab-ci.yml'
  - local: '/.gitlab/ci/static_analysis/cpp.gitlab-ci.yml'

####################################################################################################
#                                               BUILD
####################################################################################################

###############################
#          BUILD TEMPLATE
.build:ubuntu_cpp:template:
  stage: build
  needs: []
  rules:
    - allow_failure: false
  extends: .build:variable:aidge_install 
  tags:
    - docker
  before_script:
    # Download dependencies
    - !reference [.retrieve_deps:apt, script]

    - DEPENDENCY_JOB="$CI_JOB_NAME"
    - !reference [.ubuntu:download:artifacts, before_script]
    # Build current module
    - export CMAKE_PREFIX_PATH=$AIDGE_INSTALL
    - echo "Build directory :$BUILD_DIR"
    - mkdir -p $BUILD_DIR
    - echo ${AIDGE_INSTALL/"../"}
    - mkdir -p ${AIDGE_INSTALL/"../"} # strips the variable of "../"

  artifacts:
    expire_in: 3 days
    paths:
      - $BUILD_DIR/
      - $AIDGE_INSTALL_ARTIFACT/

###############################
#          BUILD JOBS
build:ubuntu_cpp:
  extends: .build:ubuntu_cpp:template
  script:
    - cd $BUILD_DIR
    # WERROR=OFF until https://gitlab.eclipse.org/eclipse/aidge/aidge/-/issues/137 is not closed
    - cmake  -DCMAKE_BUILD_TYPE=Release -DWERROR=OFF -DCOVERAGE=ON -DCMAKE_EXPORT_COMPILE_COMMANDS=1 -DPYBIND=0 ..
    - make -j4 all install

    
build:ubuntu_cpp:g++:
  stage: build
  needs: []
  extends:
    - .build:ubuntu_cpp:template
    - .rules:build:merge_pipeline_or_branch_dev_main
  tags:
    - docker
  parallel:
    matrix:
      - Gplusplus_VERSION: ["10","12"]
  script:
    - cd $BUILD_DIR
    - apt-get install -y g++-$Gplusplus_VERSION
    - export CXX=/usr/bin/g++-$Gplusplus_VERSION 
    # WERROR=OFF until https://gitlab.eclipse.org/eclipse/aidge/aidge/-/issues/137 is not closed
    - cmake -DCMAKE_BUILD_TYPE=Release -DWERROR=OFF -DCOVERAGE=OFF -DPYBIND=0 -DCMAKE_EXPORT_COMPILE_COMMANDS=1 ..
    - make -j4 all install

build:ubuntu_cpp:clang:
  stage: build
  needs: []
  extends:
    - .build:ubuntu_cpp:template
    - .rules:build:merge_pipeline_or_branch_dev_main  
  tags:
    - docker
  parallel:
    matrix:
      - CLANG_VERSION: ["12","15"]
  script:
    - cd $BUILD_DIR
    - apt-get install -y clang-$CLANG_VERSION
    # WERROR=OFF until https://gitlab.eclipse.org/eclipse/aidge/aidge/-/issues/137 is not closed
    - cmake -DCMAKE_CXX_COMPILER=/usr/bin/clang++-$CLANG_VERSION -DCMAKE_BUILD_TYPE=Release -DWERROR=OFF -DCOVERAGE=OFF -DPYBIND=0 -DCMAKE_EXPORT_COMPILE_COMMANDS=1 ..
    - make -j4 all install


####################################################################################################
#                                               TEST
####################################################################################################
test:ubuntu_cpp:
  stage: test
  needs: ["build:ubuntu_cpp"]
  extends: .build:variable:aidge_install 
  rules:
    - when : on_success
    - allow_failure: false
  tags:
    - docker
  before_script :
    # Download dependencies
    - !reference [.retrieve_deps:apt, script]

  script:
    - cd $BUILD_DIR
    - ctest --output-junit ctest-results.xml --output-on-failure
  artifacts:
    reports:
      junit: $BUILD_DIR/ctest-results.xml

####################################################################################################
#                                               COVERAGE
####################################################################################################
coverage:ubuntu_cpp:
  stage: coverage
  needs: ["build:ubuntu_cpp"]
  extends:
    - .build:variable:aidge_install 
    - .rules:static_analysis_coverage
  tags:
    - docker
  before_script:
    - !reference [.retrieve_deps:apt, script]
    - apt-get install -qq -y gcovr
  script:
    - cd $BUILD_DIR
    - ctest --output-on-failure
    # HTML report for visualization
    - gcovr --html-details --exclude-unreachable-branches -o coverage.html --root ${CI_PROJECT_DIR} --filter '\.\./include/' --filter '\.\./src/'
    # Coberta XML report for Gitlab integration
    - gcovr --xml-pretty --exclude-unreachable-branches --print-summary -o coverage.xml --root ${CI_PROJECT_DIR} --filter '\.\./include/' --filter '\.\./src/'
  coverage: /^\s*lines:\s*\d+.\d+\%/
  artifacts:
    name: ${CI_JOB_NAME}-${CI_COMMIT_REF_NAME}-${CI_COMMIT_SHA}
    expire_in: 3 days
    reports:
      coverage_report:
        coverage_format: cobertura
        path: $BUILD_DIR/coverage.xml
